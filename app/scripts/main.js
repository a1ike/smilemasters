$(document).ready(function() {
  /* $(window).on("load scroll resize", function() {
    $("body").css(
      "paddingTop",
      $(".s-header").height() > 116 ? $(".s-header").height() : 116
    );
  }); */

  new WOW().init();

  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */

  $('.phone').inputmask({
    mask: '+7(999)999-99-99',
    showMaskOnHover: false
  });

  $(document).on('load scroll resize', function() {
    if ($(this).scrollTop() > 50) {
      $('.s-header').css('background', 'rgba(0, 0, 0, 1)');
      $('.s-header').css('padding', '10px 0');
    } else {
      $('.s-header').css('background', 'transparent');
      $('.s-header').css('padding', '35px 0');
    }
  });

  $('.s-header__nav').on('click', function() {
    $('.s-nav__wrapper').toggleClass('s-nav_opened');
  });

  $('.s-nav__close').on('click', function() {
    $('.s-nav__wrapper').toggleClass('s-nav_opened');
  });

  $('.open-modal').on('click', function() {
    $('.s-modal').toggleClass('s-modal_opened');
  });

  $('.s-modal__close').on('click', function() {
    $('.s-modal').toggleClass('s-modal_opened');
  });

  $('.open-complete').on('click', function() {
    $('.s-complete').slideToggle('fast');
  });

  $('.s-complete__close').on('click', function() {
    $('.s-complete').slideToggle('fast');
  });

  $('.s-price__card-header').on('click', function() {
    $(this)
      .next()
      .slideToggle('fast');
    $(this)
      .find('.s-price__card-arrow')
      .toggleClass('s-price__card-arrow_rotated');
  });

  $('.s-top').slick({
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.s-education__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.s-task__big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    fade: true,
    asNavFor: '.s-task__small'
  });
  $('.s-task__small').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.s-task__big',
    arrows: false,
    dots: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });
});
